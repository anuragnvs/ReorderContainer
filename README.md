## Introduction
This is a cpp library that reorders a sequence container according to `task(1).md`.

## Features

* C++14 or higher
* Header-only
* Dependency-free

## Test Locally

* Using **Docker**
    * Create a Docker image from the [Dockerfile](./Dockerfile)
        ```
        docker build --tag reorder-container-cpp .
        docker run -it reorder-container-cpp:latest /bin/bash
        cd  build/bin/
        ./ReorderContainer_tests
        ```


## Integration
Since it's a header only library, you need just copy `reordercontainer.h` to your project.
