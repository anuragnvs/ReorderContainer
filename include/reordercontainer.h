#ifndef REORDERCONTAINER_H
#define REORDERCONTAINER_H

#include<iostream>
#include<algorithm>
#include<functional>

struct data
{
    uint8_t x;
    double y;
    data(uint8_t _x, double _y) {x=_x; y=_y;}
};

typedef struct data st_data;

// sort container with forward_iterator
template<typename TFAIterator>
void sortHelper(TFAIterator beg, TFAIterator end, std::forward_iterator_tag)
{
    std::cout<<"Forward Access Iterator used  to sort"<<std::endl;
    std::vector<typename TFAIterator::value_type> Temp {beg, end};
    std::sort(Temp.begin(), Temp.end(), [&](auto it1, auto it2){

        return it1.y < it2.y;});

    std::copy(Temp.begin(), Temp.end(), beg);
    
    std::cout<<"Forward Access Iterator END"<<std::endl;
}

//sort container with Random Access Iterator
template<typename TRAIterator>
void sortHelper(TRAIterator beg, TRAIterator end, std::random_access_iterator_tag)
{
    std::cout<<"Random Access Iterator used  to sort"<<std::endl;
    std::sort(beg, end, [&](auto it1, auto it2){

            return it1.y < it2.y;});
}

// sort any sequence container
template<typename TSeqContainer>
void sortSeqContainer(TSeqContainer& _contObj)
{
    sortHelper(_contObj.begin(), _contObj.end(), typename std::iterator_traits<typename TSeqContainer::iterator>::iterator_category());
}

// reorder container based on x_threshold such that subset A preceeds subset B
template<typename TSeqContainer>
void reorderContainerAB(TSeqContainer &_contObj, const uint8_t x_threshold)
{
    std::partition(_contObj.begin(), _contObj.end(), [&](auto it){
        return it.x>x_threshold;
    });
}

/// @brief 
/// @tparam TSeqContainer 
/// @param _contObj 
/// @param x_threshold 
/// @param n 
template<typename TSeqContainer>
void reorderContainerABC(TSeqContainer &_contObj, const uint8_t x_threshold, const uint64_t n)
{
    // partition into subset A and Subset B such that A precedes B
    std::partition(_contObj.begin(), _contObj.end(), [&](auto it){
        return it.x>x_threshold;
    });

    // get partition point based on threshold
    auto _ppItr = std::partition_point(_contObj.begin(), _contObj.end(), [&](auto it){
        return it.x>x_threshold;
    });

    // sort the subset B in ascending order of y

   sortHelper(_ppItr, _contObj.end(), typename std::iterator_traits<typename TSeqContainer::iterator>::iterator_category());
}

/// @brief 
/// @tparam TSeqContainer 
/// @param _contObj 
/// @param x_threshold 
/// @param n 
/// @param _myFun 
template<typename TSeqContainer>
void reorderContainerABCCallback(TSeqContainer &_contObj, const uint8_t x_threshold, const uint64_t n, std::function<void(st_data&)> _myFun)
{
    // partition into subset A and Subset B such that A precedes B
    std::partition(_contObj.begin(), _contObj.end(), [&](auto it){
        return it.x>x_threshold;
    });

    // get partition point based on threshold
    auto _ppItr = std::partition_point(_contObj.begin(), _contObj.end(), [&](auto it){
        return it.x>x_threshold;
    });

    // sort the subset B in ascending order of y
    sortHelper(_ppItr, _contObj.end(), typename std::iterator_traits<typename TSeqContainer::iterator>::iterator_category());
    int _count = std::distance(_ppItr, _contObj.end());
    if(_count < n)
    {
    	throw std::invalid_argument( "received value of n is more than the size of subset B" );
    }
    uint64_t count = 0;
    for(;_ppItr!=_contObj.end() && count++ < n; ++_ppItr)
    {
        _myFun(*_ppItr);
    }
    std::cout<<std::endl;
}



#endif // REORDERCONTAINER_H
