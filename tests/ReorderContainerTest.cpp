#include "reordercontainer.h"
#include <bits/stdc++.h>
#include <gtest/gtest.h>

namespace{

   st_data d1(1,1.0);
   st_data d2(2,2.0);
   st_data d3(3,3.0);
   st_data d4(4,4.0);
   st_data d5(8,5.0);
   st_data d6(10,7.0);
   st_data d7(12,6.0);
   uint8_t x_threshold = 4;
   uint64_t n=2;
   //_data.x+=100;
   //_data.y+=100;
   
   template <typename TItr>
   bool isSameSize(TItr it1Beg, TItr it1End, TItr it2Beg,TItr it2End)
   {
      int _countA = std::distance(it1Beg, it1End); // Actual
      int _countE = std::distance(it2Beg, it2End); // Expected
      if(_countA != _countE)
      {
    	 throw std::invalid_argument( "Invalid Test Case A" );
    	 return false;
      }
      return true;
   }
   
   template <typename TSeqContainer>
   bool seqContainerTester(TSeqContainer& _containerIn, TSeqContainer& _listA, TSeqContainer& _listB, TSeqContainer& _listC)
   {
      reorderContainerABC(_containerIn, x_threshold, n);
      auto _ppItr = std::partition_point(_containerIn.begin(), _containerIn.end(), [&](auto it){
        return it.x>x_threshold;
      });

      // test for A
      /*
      int _countA = std::distance(_containerIn.begin(), _ppItr); // Actual
      int _countE = std::distance(_listA.begin(), _listA.end()); // Expected
      
      if(_countA != _countE)
      {
    	 throw std::invalid_argument( "Invalid Test Case A" );
    	 return false;
      } */
      EXPECT_TRUE(isSameSize(_containerIn.begin(), _ppItr, _listA.begin(), _listA.end()));
      EXPECT_TRUE(std::is_permutation(_containerIn.begin(), _ppItr,  _listA.begin(), [&](auto it1, auto it2){

            return (it1.x == it2.x && it1.y == it2.y);}));
      
      // test for subset B
      /*
      _countA = std::distance( _ppItr, _containerIn.end()); // Actual
      _countE = std::distance(_listB.begin(), _listB.end()); // Expected
      if(_countA != _countE)
      {
    	 throw std::invalid_argument( "Invalid Test Case B" );
    	 return false;
      }
      */
      EXPECT_TRUE(isSameSize(_ppItr, _containerIn.end(), _listB.begin(), _listB.end()));
      EXPECT_TRUE(std::is_permutation(_ppItr, _containerIn.end(),  _listB.begin(), [&](auto it1, auto it2){

            return (it1.x == it2.x && it1.y == it2.y);}));

      // test for C
      auto _ppItrEnd = _ppItr;
      for(uint64_t i=0; i < n && _ppItrEnd!=_containerIn.end(); i++)
      {
      	_ppItrEnd++;

      }
      
      /*
      _countA = std::distance( _ppItr, _ppItrEnd); // Actual
      _countE = std::distance(_listC.begin(), _listC.end()); // Expected
      if(_countA != _countE)
      {
    	 throw std::invalid_argument( "Invalid Test Case C" );
    	 return false;
      }
      */
      EXPECT_TRUE(isSameSize(_ppItr, _ppItrEnd, _listC.begin(), _listC.end()));
      EXPECT_TRUE(std::is_permutation(_ppItr, _ppItrEnd,  _listC.begin(), [&](auto it1, auto it2){

            return (it1.x == it2.x && it1.y == it2.y);}));

   	for(st_data& item:_listC)
   	{
         item.x +=200.0;
   		item.y +=200.0;
   	}

      // test for C
      _ppItrEnd = std::partition_point(_containerIn.begin(), _containerIn.end(), [&](auto it){
        return it.x>x_threshold;
      });
      
      reorderContainerABCCallback(_containerIn, x_threshold, n, [](st_data& _data){
         _data.x+=200;
         _data.y+=200;
   	});

      for(uint64_t i=0;  _ppItrEnd!=_containerIn.end() && i< n; i++)
      _ppItrEnd++;
      
      /*
      _countA = std::distance( _ppItr, _ppItrEnd); // Actual
      _countE = std::distance(_listC.begin(), _listC.end()); // Expected
      if(_countA != _countE)
      {
    	 throw std::invalid_argument( "Invalid Test Case C" );
    	 return false;
      }
      */
      EXPECT_TRUE(isSameSize(_ppItr, _ppItrEnd, _listC.begin(), _listC.end()));
      EXPECT_TRUE(std::is_permutation(_ppItr, _ppItrEnd,  _listC.begin(), [&](auto it1, auto it2){

            return (it1.x == it2.x && it1.y == it2.y);}));

      return true;
   }

   TEST(ReorderContainer, vector)
   {
      std::vector<st_data> _containerIn {d1,d2,d3,d4,d5,d6,d7};
   	std::vector<st_data> _listA{d7,d6,d5};
   	
   	std::vector<st_data> _listB{d1,d2,d3,d4};
   	std::vector<st_data> _listC{d1,d2};
      EXPECT_TRUE(seqContainerTester(_containerIn, _listA, _listB, _listC));
   }

   TEST(ReorderContainer, list)
   {
      std::list<st_data> _containerIn {d1,d2,d3,d4,d5,d6,d7};
   	std::list<st_data> _listA{d7,d6,d5};
   	
   	std::list<st_data> _listB{d1,d2,d3,d4};
   	std::list<st_data> _listC{d1,d2};
      EXPECT_TRUE(seqContainerTester(_containerIn, _listA, _listB, _listC));
   }

   TEST(ReorderContainer, deque)
   {
      std::deque<st_data> _containerIn {d1,d2,d3,d4,d5,d6,d7};
   	std::deque<st_data> _listA{d7,d6,d5};
   	
   	std::deque<st_data> _listB{d1,d2,d3,d4};
   	std::deque<st_data> _listC{d1,d2};
      EXPECT_TRUE(seqContainerTester(_containerIn, _listA, _listB, _listC));
   }

   TEST(ReorderContainer, forward_list)
   {
      std::forward_list<st_data> _containerIn {d1,d2,d3,d4,d5,d6,d7};
   	std::forward_list<st_data> _listA{d7,d6,d5};
   	
   	std::forward_list<st_data> _listB{d1,d2,d3,d4};
   	std::forward_list<st_data> _listC{d1,d2};
      EXPECT_TRUE(seqContainerTester(_containerIn, _listA, _listB, _listC));
   }

   // TEST(ReorderContainer, array)
   // {
   //    std::array<st_data,7> _containerIn {d1,d2,d3,d4,d5,d6,d7};
   // 	std::array<st_data,3> _listA{d7,d6,d5};
   	
   // 	std::array<st_data,4> _listB{d1,d2,d3,d4};
   // 	std::array<st_data,2> _listC{d1,d2};
   //    EXPECT_TRUE(seqContainerTester(_containerIn, _listA, _listB, _listC));
   // }

//    TEST(ReorderContainer, vector)
//    {
//    	std::vector<st_data> _containerIn {d1,d2,d3,d4,d5,d6,d7};
//    	std::vector<st_data> _listA{d7,d6,d5};
   	
//    	std::vector<st_data> _listB{d1,d2,d3,d4};
//    	std::vector<st_data> _listC{d1,d2};
//       reorderContainerABC(_containerIn, x_threshold, n);
//       auto _ppItr = std::partition_point(_containerIn.begin(), _containerIn.end(), [&](auto it){
//         return it.x>x_threshold;
//       });

//       // test for A
//       EXPECT_TRUE(std::is_permutation(_containerIn.begin(), _ppItr,  _listA.begin(), [&](auto it1, auto it2){

//             return (it1.x == it2.x && it1.y == it2.y);}));
      
//       // test for subset B
//       EXPECT_TRUE(std::is_permutation(_ppItr, _containerIn.end(),  _listB.begin(), [&](auto it1, auto it2){

//             return (it1.x == it2.x && it1.y == it2.y);}));

//         // test for C
//       auto _ppItrEnd = _ppItr;
//       for(uint64_t i=0; i< n; i++)
//       _ppItrEnd++;
//       EXPECT_TRUE(std::is_permutation(_ppItr, _ppItrEnd,  _listC.begin(), [&](auto it1, auto it2){

//             return (it1.x == it2.x && it1.y == it2.y);}));

//    	for(st_data& item:_listC)
//    	{
//          item.x +=200.0;
//    		item.y +=200.0;
//    	}
   	
   	
   	
//    	// test for C
//       _ppItrEnd = std::partition_point(_containerIn.begin(), _containerIn.end(), [&](auto it){
//         return it.x>x_threshold;
//       });
      
//       reorderContainerABCCallback(_containerIn, 4, 2, [](st_data& _data){
//          _data.x+=200;
//          _data.y+=200;
//    	});

//       for(uint64_t i=0; i< n; i++)
//       _ppItrEnd++;
//       EXPECT_TRUE(std::is_permutation(_ppItr, _ppItrEnd,  _listC.begin(), [&](auto it1, auto it2){

//             return (it1.x == it2.x && it1.y == it2.y);}));
//    }
   
//    TEST(ReorderContainer, forward_list)
//    {
//    	std::forward_list<st_data> _containerIn {d1,d2,d3,d4,d5,d6,d7};
//    	std::forward_list<st_data> _listA{d7,d6,d5};
   	
//    	std::forward_list<st_data> _listB{d1,d2,d3,d4};
//    	std::forward_list<st_data> _listC{d1,d2};
//       reorderContainerABC(_containerIn, x_threshold,n);
//       auto _ppItr = std::partition_point(_containerIn.begin(), _containerIn.end(), [&](auto it){
//         return it.x>x_threshold;
//       });

//       // test for A
//       EXPECT_TRUE(std::is_permutation(_containerIn.begin(), _ppItr,  _listA.begin(), [&](auto it1, auto it2){

//             return (it1.x == it2.x && it1.y == it2.y);}));
      
//       // test for subset B
//       EXPECT_TRUE(std::is_permutation(_ppItr, _containerIn.end(),  _listB.begin(), [&](auto it1, auto it2){

//             return (it1.x == it2.x && it1.y == it2.y);}));

//         // test for C
//       auto _ppItrEnd = _ppItr;
//       for(uint64_t i=0; i< n; i++)
//       _ppItrEnd++;
//       EXPECT_TRUE(std::is_permutation(_ppItr, _ppItrEnd,  _listC.begin(), [&](auto it1, auto it2){

//             return (it1.x == it2.x && it1.y == it2.y);}));

//    	for(st_data& item:_listC)
//    	{
//          item.x +=200.0;
//    		item.y +=200.0;
//    	}
   	
   	
   	
//    	// test for C
//       _ppItrEnd = std::partition_point(_containerIn.begin(), _containerIn.end(), [&](auto it){
//         return it.x>x_threshold;
//       });
      
//       reorderContainerABCCallback(_containerIn, 4, 2, [](st_data& _data){
//          _data.x+=200;
//          _data.y+=200;
//    	});

//       for(uint64_t i=0; i< n; i++)
//       _ppItrEnd++;
//       EXPECT_TRUE(std::is_permutation(_ppItr, _ppItrEnd,  _listC.begin(), [&](auto it1, auto it2){

//             return (it1.x == it2.x && it1.y == it2.y);}));

//    }
   

// TEST(ReorderContainer, list)
//    {
//    	std::list<st_data> _containerIn {d1,d2,d3,d4,d5,d6,d7};
//    	std::list<st_data> _listA{d7,d6,d5};
   	
//    	std::list<st_data> _listB{d1,d2,d3,d4};
//    	std::list<st_data> _listC{d1,d2};
//       reorderContainerABC(_containerIn, x_threshold, n);
//       auto _ppItr = std::partition_point(_containerIn.begin(), _containerIn.end(), [&](auto it){
//         return it.x>x_threshold;
//       });

//       // test for A
//       EXPECT_TRUE(std::is_permutation(_containerIn.begin(), _ppItr,  _listA.begin(), [&](auto it1, auto it2){

//             return (it1.x == it2.x && it1.y == it2.y);}));
      
//       // test for subset B
//       EXPECT_TRUE(std::is_permutation(_ppItr, _containerIn.end(),  _listB.begin(), [&](auto it1, auto it2){

//             return (it1.x == it2.x && it1.y == it2.y);}));

//         // test for C
//       auto _ppItrEnd = _ppItr;
//       for(uint64_t i=0; i< n; i++)
//       _ppItrEnd++;
//       EXPECT_TRUE(std::is_permutation(_ppItr, _ppItrEnd,  _listC.begin(), [&](auto it1, auto it2){

//             return (it1.x == it2.x && it1.y == it2.y);}));

//    	for(st_data& item:_listC)
//    	{
//          item.x +=200.0;
//    		item.y +=200.0;
//    	}
   	
   	
   	
//    	// test for C
//       _ppItrEnd = std::partition_point(_containerIn.begin(), _containerIn.end(), [&](auto it){
//         return it.x>x_threshold;
//       });
      
//       reorderContainerABCCallback(_containerIn, 4, 2, [](st_data& _data){
//          _data.x+=200;
//          _data.y+=200;
//    	});

//       for(uint64_t i=0; i< n; i++)
//       _ppItrEnd++;
//       EXPECT_TRUE(std::is_permutation(_ppItr, _ppItrEnd,  _listC.begin(), [&](auto it1, auto it2){

//             return (it1.x == it2.x && it1.y == it2.y);}));
//    }

//    TEST(ReorderContainer, deque)
//    {
//    	std::deque<st_data> _containerIn {d1,d2,d3,d4,d5,d6,d7};
//    	std::deque<st_data> _listA{d7,d6,d5};
   	
//    	std::deque<st_data> _listB{d1,d2,d3,d4};
//    	std::deque<st_data> _listC{d1,d2};
//       reorderContainerABC(_containerIn, x_threshold, n);
//       auto _ppItr = std::partition_point(_containerIn.begin(), _containerIn.end(), [&](auto it){
//         return it.x>x_threshold;
//       });

//       // test for A
//       EXPECT_TRUE(std::is_permutation(_containerIn.begin(), _ppItr,  _listA.begin(), [&](auto it1, auto it2){

//             return (it1.x == it2.x && it1.y == it2.y);}));
      
//       // test for subset B
//       EXPECT_TRUE(std::is_permutation(_ppItr, _containerIn.end(),  _listB.begin(), [&](auto it1, auto it2){

//             return (it1.x == it2.x && it1.y == it2.y);}));

//         // test for C
//       auto _ppItrEnd = _ppItr;
//       for(uint64_t i=0; i< n; i++)
//       _ppItrEnd++;
//       EXPECT_TRUE(std::is_permutation(_ppItr, _ppItrEnd,  _listC.begin(), [&](auto it1, auto it2){

//             return (it1.x == it2.x && it1.y == it2.y);}));

//    	for(st_data& item:_listC)
//    	{
//          item.x +=200.0;
//    		item.y +=200.0;
//    	}
   	
//    	// test for C
//       _ppItrEnd = std::partition_point(_containerIn.begin(), _containerIn.end(), [&](auto it){
//         return it.x>x_threshold;
//       });
      
//       reorderContainerABCCallback(_containerIn, 4, 2, [](st_data& _data){
//          _data.x+=200;
//          _data.y+=200;
//    	});

//       for(uint64_t i=0; i< n; i++)
//       _ppItrEnd++;
//       EXPECT_TRUE(std::is_permutation(_ppItr, _ppItrEnd,  _listC.begin(), [&](auto it1, auto it2){

//             return (it1.x == it2.x && it1.y == it2.y);}));
//    }
   
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
